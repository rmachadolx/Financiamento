﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Financiamento
{
    public class FinanciamentoDeCarro : Financiamento
    {
        private double _comissaoAbertura;
        private int _prazo;
        private double _valorResidual;
        public bool validaPrazo { get; set; }
        public string Mensagem { get; private set; }

        public double comissaoAbertura
        {
            get { return _comissaoAbertura; }
            set {_comissaoAbertura=value*0.0001; }
        } 
        public double valorResidual {
            get { return _valorResidual; }
            set { _valorResidual = value * 0.01;  }
        }
    

        public override int Prazo
        {
            get
            {
                return _prazo;
            }

            set
            {
                if (value>0 && value<=60)
                {
                    _prazo = value;
                    validaPrazo = true;
                    Mensagem = "Prazo válido";   
                }
                else
                {
                    validaPrazo = false;
                    Mensagem = "O prazo tem de ser entre 1 e 60 meses";
                  
                }
                
                ;
            }
        }

        public FinanciamentoDeCarro(double montante, int prazo, double taxaAnualBruta) : base(montante, prazo, taxaAnualBruta)
        {
            comissaoAbertura= montante;
            valorResidual = montante;  
            
        }
        public override string ToString()
        {
            return $"Finaciamento Automóvel-> {base.ToString()} \nValor da Prestação:{CalcularPrestacao():C2} \nComissão de abertura:{comissaoAbertura:C2} \nÚltima prestação:{valorResidual:C2} ";
        }
        

        public override double CalcularPrestacao()
        {

            return ((Montante-valorResidual + Montante*TaxaAnualBruta/100)/(Prazo-1));
        }

     
    }

}
