﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Financiamento
{
    public abstract class Financiamento
    {
       
        public double  Montante { get; set; }

        public abstract int Prazo  { get; set; }

        public double TaxaAnualBruta { get; set; }

        public Financiamento(double montante, int prazo, double taxaAnualBruta)
        {
            Montante = montante;
            Prazo = prazo;
            TaxaAnualBruta = taxaAnualBruta;
        }
        public override string ToString()
        {
            return $"Montante:{Montante:C2}  Prazo:{Prazo}  Taxa:{TaxaAnualBruta}%";
        }
        public abstract double CalcularPrestacao();


    }
}
