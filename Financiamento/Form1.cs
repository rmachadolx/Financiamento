﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Financiamento
{
    public partial class Form1 : Form
    {

        FinanciamentoDeCarro financiamentoDeCarro;
        public Form1()
        {
            InitializeComponent();
        }



        private void ButtonCalcular_Click(object sender, EventArgs e)
        {

            switch (ComboBoxTipoFinanciamento.SelectedIndex)
            {
                case 0:
                    {

                        financiamentoDeCarro = new FinanciamentoDeCarro(Convert.ToDouble(TextBoxMontante.Text), Convert.ToInt32(NumericUpDownPrestacoes.Text), Convert.ToDouble(TextBoxTaxaAnualBruta.Text));
                        if (financiamentoDeCarro.validaPrazo)
                        {
                            LabelFinanciamento.Text = financiamentoDeCarro.ToString();
                        }
                        else
                        {
                            MessageBox.Show(financiamentoDeCarro.Mensagem);
                        }


                    }
                    break;
            }
        }
    }
}
