﻿namespace Financiamento
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComboBoxTipoFinanciamento = new System.Windows.Forms.ComboBox();
            this.L = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxMontante = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.NumericUpDownPrestacoes = new System.Windows.Forms.NumericUpDown();
            this.LabelFinanciamento = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TextBoxTaxaAnualBruta = new System.Windows.Forms.TextBox();
            this.ButtonCalcular = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownPrestacoes)).BeginInit();
            this.SuspendLayout();
            // 
            // ComboBoxTipoFinanciamento
            // 
            this.ComboBoxTipoFinanciamento.FormattingEnabled = true;
            this.ComboBoxTipoFinanciamento.Items.AddRange(new object[] {
            "Automóvel",
            "Habitação",
            "Outros"});
            this.ComboBoxTipoFinanciamento.Location = new System.Drawing.Point(203, 63);
            this.ComboBoxTipoFinanciamento.Name = "ComboBoxTipoFinanciamento";
            this.ComboBoxTipoFinanciamento.Size = new System.Drawing.Size(121, 21);
            this.ComboBoxTipoFinanciamento.TabIndex = 0;
            // 
            // L
            // 
            this.L.AutoSize = true;
            this.L.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.L.Location = new System.Drawing.Point(401, 63);
            this.L.Name = "L";
            this.L.Size = new System.Drawing.Size(75, 16);
            this.L.TabIndex = 1;
            this.L.Text = "Montante:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(62, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Financiamento:";
            // 
            // TextBoxMontante
            // 
            this.TextBoxMontante.Location = new System.Drawing.Point(516, 61);
            this.TextBoxMontante.Name = "TextBoxMontante";
            this.TextBoxMontante.Size = new System.Drawing.Size(121, 20);
            this.TextBoxMontante.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(401, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Prestações:";
            // 
            // NumericUpDownPrestacoes
            // 
            this.NumericUpDownPrestacoes.Location = new System.Drawing.Point(516, 119);
            this.NumericUpDownPrestacoes.Name = "NumericUpDownPrestacoes";
            this.NumericUpDownPrestacoes.Size = new System.Drawing.Size(40, 20);
            this.NumericUpDownPrestacoes.TabIndex = 5;
            // 
            // LabelFinanciamento
            // 
            this.LabelFinanciamento.AutoSize = true;
            this.LabelFinanciamento.Location = new System.Drawing.Point(62, 274);
            this.LabelFinanciamento.Name = "LabelFinanciamento";
            this.LabelFinanciamento.Size = new System.Drawing.Size(11, 13);
            this.LabelFinanciamento.TabIndex = 6;
            this.LabelFinanciamento.Text = "*";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(62, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "Taxa Anual Bruta:";
            // 
            // TextBoxTaxaAnualBruta
            // 
            this.TextBoxTaxaAnualBruta.Location = new System.Drawing.Point(203, 115);
            this.TextBoxTaxaAnualBruta.Name = "TextBoxTaxaAnualBruta";
            this.TextBoxTaxaAnualBruta.Size = new System.Drawing.Size(54, 20);
            this.TextBoxTaxaAnualBruta.TabIndex = 9;
            // 
            // ButtonCalcular
            // 
            this.ButtonCalcular.Location = new System.Drawing.Point(516, 202);
            this.ButtonCalcular.Name = "ButtonCalcular";
            this.ButtonCalcular.Size = new System.Drawing.Size(75, 23);
            this.ButtonCalcular.TabIndex = 10;
            this.ButtonCalcular.Text = "Calcular";
            this.ButtonCalcular.UseVisualStyleBackColor = true;
            this.ButtonCalcular.Click += new System.EventHandler(this.ButtonCalcular_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 521);
            this.Controls.Add(this.ButtonCalcular);
            this.Controls.Add(this.TextBoxTaxaAnualBruta);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LabelFinanciamento);
            this.Controls.Add(this.NumericUpDownPrestacoes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TextBoxMontante);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.L);
            this.Controls.Add(this.ComboBoxTipoFinanciamento);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownPrestacoes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ComboBoxTipoFinanciamento;
        private System.Windows.Forms.Label L;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxMontante;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown NumericUpDownPrestacoes;
        private System.Windows.Forms.Label LabelFinanciamento;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TextBoxTaxaAnualBruta;
        private System.Windows.Forms.Button ButtonCalcular;
    }
}

